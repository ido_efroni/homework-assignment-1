from bs4 import BeautifulSoup
import requests
import pytest
import re


# this test/function check if the string "Right-click in the box below to see one called \'the-internet\'" contain in
# the website and return a response that contain all of the different occurrence of the string
@pytest.mark.one
def test_one():
    item_url = "https://the-internet.herokuapp.com/context_menu"
    response = requests.get(item_url)
    soup = BeautifulSoup(response.text, 'html.parser')
    string_test_one = "Right-click in the box below to see one called \'the-internet\'"
    test = soup.findAll(text=re.compile(string_test_one))
    assert len(test) > 0 and string_test_one in test[0]

# this test/function check if the string "Alibaba" contain in
# the website and return a response that contain all of the different occurrence of the string
@pytest.mark.two
def test_two():
    item_url = "https://the-internet.herokuapp.com/context_menu"
    response = requests.get(item_url)
    soup = BeautifulSoup(response.text, 'html.parser')
    string_test_two = "Alibaba"
    test = soup.findAll(text=re.compile(string_test_two))
    assert len(test) > 0 and string_test_two in test[0]
